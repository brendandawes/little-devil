//
//  FlipsideViewController.m
//  Little Devil
//
//  Created by Brendan Dawes on 13/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import "FlipsideViewController.h"
#import "FormKit.h"
#import "Settings.h"


@interface FlipsideViewController ()

@end

@implementation FlipsideViewController

@synthesize tableView;
@synthesize settings = _settings;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    self.formModel = [FKFormModel formTableModelForTableView:self.tableView
                                        navigationController:self.navigationController];
    Settings *settings = [Settings settingsWithDeviceURL:[prefs stringForKey:@"deviceURL"]];
    settings.button1Label  = [prefs stringForKey:@"button1Label"];
    settings.button1Key  = [prefs stringForKey:@"button1Key"];
    settings.button1Value  = [prefs stringForKey:@"button1Value"];
    settings.button2Label  = [prefs stringForKey:@"button2Label"];
    settings.button2Key  = [prefs stringForKey:@"button2Key"];
    settings.button2Value  = [prefs stringForKey:@"button2Value"];
    settings.button3Label  = [prefs stringForKey:@"button3Label"];
    settings.button3Key  = [prefs stringForKey:@"button3Key"];
    settings.button3Value  = [prefs stringForKey:@"button3Value"];
    settings.switch1Label  = [prefs stringForKey:@"switch1Label"];
    settings.switch1Key  = [prefs stringForKey:@"switch1Key"];
    settings.switch2Label  = [prefs stringForKey:@"switch2Label"];
    settings.switch2Key  = [prefs stringForKey:@"switch2Key"];
    settings.sliderLabel  = [prefs stringForKey:@"sliderLabel"];
     settings.sliderKey = [prefs stringForKey:@"sliderKey"];
    self.settings = settings;
    
    [FKFormMapping mappingForClass:[Settings class] block:^(FKFormMapping *formMapping) {
        
        [formMapping sectionWithTitle:@"Device" identifier:@"device"];
        [formMapping mapAttribute:@"deviceURL" title:@"URL" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Button 1" identifier:@"buton1"];
        [formMapping mapAttribute:@"button1Label" title:@"Label" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"button1Key" title:@"Key" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"button1Value" title:@"Value" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Button 2" identifier:@"buton2"];
        [formMapping mapAttribute:@"button2Label" title:@"Label" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"button2Key" title:@"Key" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"button2Value" title:@"Value" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Button 3" identifier:@"buton3"];
        [formMapping mapAttribute:@"button3Label" title:@"Label" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"button3Key" title:@"Key" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"button3Value" title:@"Value" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Switch 1" identifier:@"switch1"];
        [formMapping mapAttribute:@"switch1Label" title:@"Label" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"switch1Key" title:@"Key" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Switch 2" identifier:@"switch2"];
        [formMapping mapAttribute:@"switch2Label" title:@"Label" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"switch2Key" title:@"Key" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Slider" footer:@"Created by Brendan Dawes \nbrendandawes.com" identifier:@"slider"];
        [formMapping mapAttribute:@"sliderLabel" title:@"Label" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"sliderKey" title:@"Key" type:FKFormAttributeMappingTypeText];
        
        
        [self.formModel registerMapping:formMapping];
        
    }];
    
    [self.formModel setDidChangeValueWithBlock:^(id object, id value, NSString *keyPath) {
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:value forKey:keyPath];
        [prefs synchronize];
        
    }];
    
    [self.formModel loadFieldsWithObject:settings];

    //[self loadSettings];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [tableView endEditing:YES];
    [self.delegate flipsideViewControllerDidFinish:self];
    
}

@end
