//
//  AppDelegate.h
//  Little Devil
//
//  Created by Brendan Dawes on 13/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
