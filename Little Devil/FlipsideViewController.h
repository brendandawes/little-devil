//
//  FlipsideViewController.h
//  Little Devil
//
//  Created by Brendan Dawes on 13/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import <UIKit/UIKit.h>


@class FlipsideViewController;
@class FKFormModel;
@class Settings;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

@interface FlipsideViewController : UIViewController {
    
   
    UITableView *tableView;
}

@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;
@property (nonatomic, retain) IBOutlet UITextField *deviceUrl;



@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, strong) FKFormModel *formModel;

@property (nonatomic, strong) Settings *settings;

- (IBAction)done:(id)sender;



@end
