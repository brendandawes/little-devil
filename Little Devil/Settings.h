//
//  Settings.h
//  Little Devil
//
//  Created by Brendan Dawes on 17/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject

@property (nonatomic, retain) NSString *deviceURL;

@property (nonatomic, retain) NSString *button1Label;
@property (nonatomic, retain) NSString *button1Key;
@property (nonatomic, retain) NSString *button1Value;

@property (nonatomic, retain) NSString *button2Label;
@property (nonatomic, retain) NSString *button2Key;
@property (nonatomic, retain) NSString *button2Value;

@property (nonatomic, retain) NSString *button3Label;
@property (nonatomic, retain) NSString *button3Key;
@property (nonatomic, retain) NSString *button3Value;

@property (nonatomic, retain) NSString *switch1Label;
@property (nonatomic, retain) NSString *switch1Key;

@property (nonatomic, retain) NSString *switch2Label;
@property (nonatomic, retain) NSString *switch2Key;

@property (nonatomic, retain) NSString *sliderLabel;
@property (nonatomic, retain) NSString *sliderKey;

+ (id)settingsWithDeviceURL:(NSString *)deviceURL;


@end
