//
//  Settings.m
//  Little Devil
//
//  Created by Brendan Dawes on 17/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import "Settings.h"

@implementation Settings

@synthesize deviceURL,button1Label,button1Key,button1Value,button2Label,button2Key,button2Value,button3Label,button3Key,button3Value,switch1Label,switch1Key,switch2Label,switch2Key,sliderLabel,sliderKey;

+ (id)settingsWithDeviceURL:(NSString *)deviceURL {
    Settings *settings = [[Settings alloc] init];
    
    settings.deviceURL = deviceURL;
    
    return settings;
}

@end
