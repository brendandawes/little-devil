//
//  Constants.h
//  Little Devil
//
//  Created by Brendan Dawes on 15/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#ifndef Little_Devil_Constants_h
#define Little_Devil_Constants_h

#ifdef DEBUG
#define MyLog(str,...) NSLog(str,##__VA_ARGS__)
#else
#define MyLog(str,...)
#endif



#endif
