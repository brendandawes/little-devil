//
//  MainViewController.m
//  Little Devil
//
//  Created by Brendan Dawes on 13/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

/*
 
 Simple app to send messages to an Electric Imp
 
*/

#import "MainViewController.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "Constants.h"
#import "SVProgressHUD.h"

 

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize deviceURL,button1Label,button2Label,button3Label,switch1Label,switch2Label,accelerometer,switch1,switch2,slider,sliderLabel;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.accelerometer = [UIAccelerometer sharedAccelerometer];
    self.accelerometer.updateInterval = .2;   //update interval
    self.accelerometer.delegate = self;
    [self setDefaultUserPrefs];
    [self loadUserPrefs];
  
   
}

-(void) viewWillAppear:(BOOL)animated  {
    
    [super viewWillAppear:animated];
    
    [self setLabels];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - IBAction


- (IBAction)buttonPressed:(id)sender {
    
    NSString *key = [NSString stringWithFormat:@"%@%@", [sender restorationIdentifier], @"Key"];
    NSString *value = [NSString stringWithFormat:@"%@%@", [sender restorationIdentifier], @"Value"];
    [self sendJSONWithKey:[self getStringForKey:key] withValue:[self getStringForKey:value]];
    
}

- (IBAction)switch1Toggle:(id)sender {
    
    if(switch1.on) {
        [self sendJSONWithKey:[self getStringForKey:@"switch1Key"] withValue:@"1"];
    }
    
    else {
       [self sendJSONWithKey:[self getStringForKey:@"switch1Key"] withValue:@"0"];
    }
    
}

- (IBAction)switch2Toggle:(id)sender {
    
    if(switch2.on) {
        [self sendJSONWithKey:[self getStringForKey:@"switch2Key"] withValue:@"1"];
    }
    
    else {
        [self sendJSONWithKey:[self getStringForKey:@"switch2Key"] withValue:@"0"];
    }
    
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    
    NSString *value = [NSString stringWithFormat:@"%.1f", [sender value]];
    [self sendJSONWithKey:[self getStringForKey:@"sliderKey"] withValue:value];
    
    
    
}

#pragma mark - Settings

- (void) loadUserPrefs{
    
    deviceURL = [self getStringForKey:@"deviceURL"];
    
    
}

- (void) setLabels {
    
    button1Label.text = [self getStringForKey:@"button1Label"];
    button2Label.text = [self getStringForKey:@"button2Label"];
    button3Label.text = [self getStringForKey:@"button3Label"];
    switch1Label.text = [self getStringForKey:@"switch1Label"];
    switch2Label.text = [self getStringForKey:@"switch2Label"];
    sliderLabel.text = [self getStringForKey:@"sliderLabel"];
    
}

- (void) setDefaultUserPrefs{
    
    NSDictionary *defaults = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"https://agent.electricimp.com/", @"deviceURL",
                              @"button 1",@"button1Label",
                              @"message",@"button1Key",
                              @"1",@"button1Value",
                              @"button 2",@"button2Label",
                              @"message",@"button2Key",
                              @"1",@"button2Value",
                              @"button 3",@"button3Label",
                              @"message",@"button3Key",
                              @"1",@"button3Value",
                              @"switch 1",@"switch1Label",
                              @"switch1",@"switch1Key",
                              @"switch 2",@"switch2Label",
                              @"switch2",@"switch2Key",
                              @"slider",@"sliderLabel",
                              @"slider",@"sliderKey",
                              nil];
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults registerDefaults:defaults];
    [userdefaults synchronize];
    

    
}

- (NSString *) getStringForKey:(NSString *) key {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *val = [prefs stringForKey:key];
    return val;

}

#pragma mark - Accelerometer


- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration{
    //NSLog(@"acceleration x : %f",acceleration.x);    //xAxis
   // NSLog(@"acceleration y : %f",acceleration.y);    //yAxis
    //NSLog(@"acceleration z : %f",acceleration.z);    //zAxis
}

#pragma mark - POST

- (void)sendJSONWithKey:(NSString *)key withValue:(NSString *) value{
    
    if ([self isDeviceURLSet]) {
    
    AFHTTPClient *httpClient=[[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:@"https://agent.electricimp.com/"]];
    
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
   
    NSMutableURLRequest *request=[httpClient requestWithMethod:@"POST"
                                            path:[self getStringForKey:@"deviceURL"]
                                            parameters:@{key:value}];
    
    AFHTTPRequestOperation *operation=[[AFHTTPRequestOperation alloc]initWithRequest:request];
    
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
                                    failure:^(AFHTTPRequestOperation*operation,NSError*error){
                                        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                    }];
       [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [operation start];
    } else {
        
        [SVProgressHUD showErrorWithStatus:@"No URL set!"];
    }
    
    
}

- (BOOL) isDeviceURLSet{
    
    if([[self getStringForKey:@"deviceURL"] length] == 0) {
        return NO;
    }   else {
        return YES;
    }
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

@end
