//
//  MainViewController.h
//  Little Devil
//
//  Created by Brendan Dawes on 13/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import "FlipsideViewController.h"



@interface MainViewController : UIViewController <FlipsideViewControllerDelegate,UIAccelerometerDelegate> {
    
    NSString *deviceURL;
    UILabel *button1Label;
    UILabel *button2Label;
    UILabel *button3Label;
    UILabel *switch1Label;
    UILabel *switch2Label;
    UILabel *sliderLabel;
    UISwitch *switch1;
    UISwitch *switch2;
    UISlider *slider;
    UIAccelerometer *accelerometer;



   
}
@property(nonatomic,retain)UIAccelerometer *accelerometer;
@property (nonatomic, retain) NSString *deviceURL;
@property (nonatomic, retain) IBOutlet UISwitch *switch1;
@property (nonatomic, retain) IBOutlet UISwitch *switch2;
@property (nonatomic, retain) IBOutlet UILabel *button1Label;
@property (nonatomic, retain) IBOutlet UILabel *button2Label;
@property (nonatomic, retain) IBOutlet UILabel *button3Label;
@property (nonatomic, retain) IBOutlet UILabel *switch1Label;
@property (nonatomic, retain) IBOutlet UILabel *switch2Label;
@property (nonatomic, retain) IBOutlet UILabel *sliderLabel;
@property (nonatomic, retain) IBOutlet UISlider *slider;


- (IBAction)buttonPressed:(id)sender;

- (IBAction)switch1Toggle:(id)sender;

- (IBAction)switch2Toggle:(id)sender;

- (IBAction) sliderValueChanged:(id)sender;

- (void) setDefaultUserPrefs;

- (void)sendJSONWithKey:(NSString *)key withValue:(NSString *) value;

- (NSString *) getStringForKey:(NSString *) key;

- (BOOL) isDeviceURLSet;

@end
