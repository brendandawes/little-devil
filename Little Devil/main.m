//
//  main.m
//  Little Devil
//
//  Created by Brendan Dawes on 13/07/2013.
//  Copyright (c) 2013 Brendan Dawes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
