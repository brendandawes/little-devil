Little Devil for Electric Imp
-----------------------------------

By [Brendan Dawes] (http://brendandawes.com)

Version 1.0 

Little Devil is a simple controller for Electric Imp devices for iOS. The Dieter Rams inspired interface comes with three push buttons, two switches and one slider. Each control sends a JSON key/value pair to any device URL as a POST.

Using the settings screen you can set the device URL and the key/value pairs for each control though note the switches always send either 1 or 0 dependending on the switches state. The slider sends a floating point number between 0.0 and 1.0.

Tapping on the name of a control in the settings screen will allow you to edit the name of the control.

Little Devil is released under The MIT License (MIT)

The MIT License (MIT)

Copyright (c) 2013 Brendan Jay Dawes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

